﻿using Sandbox;
using SWB_Base;

namespace SWB_CSS
{
    [Library("swb_css_stg44", Title = "STG44")]
    public class STG44 : WeaponBase
    {
        public override int Bucket => 3;
        public override HoldType HoldType => HoldType.Rifle;
        public override string ViewModelPath => "weapons/stg44/v_stg44.vmdl";
        public override string WorldModelPath => "weapons/stg44/w2_stg44.vmdl";
        public override string Icon => "";
        public override int FOV => 75;
        public override int ZoomFOV => 60;
        public override float WalkAnimationSpeedMod => 0.85f;

        public STG44()
        {
            Primary = new ClipInfo
            {
                Ammo = 30,
                AmmoType = AmmoType.Rifle,
                ClipSize = 30,
                ReloadTime = 2.17f,

                BulletSize = 4f,
                Damage = 15f,
                Force = 3f,
                Spread = 0.1f,
                Recoil = 0.5f,
                RPM = 600,
                FiringType = FiringType.auto,
                ScreenShake = new ScreenShake
                {
                    Length = 0.5f,
                    Speed = 4.0f,
                    Size = 0.5f,
                    Rotation = 0.5f
                },

                DryFireSound = "swb_rifle.empty",
                ShootSound = "stg44_fp",

                BulletEjectParticle = "particles/pistol_ejectbrass.vpcf",
                MuzzleFlashParticle = "particles/pistol_muzzleflash.vpcf",

                InfiniteAmmo = InfiniteAmmoType.reserve
            };

            ZoomAnimData = new AngPos
            {
                Angle = new Angles(-0.2f, -0.01f, 0),
                Pos = new Vector3(-2.316f, -4.5f, 0.58f)
            };

            RunAnimData = new AngPos
            {
                Angle = new Angles(10, 40, 0),
                Pos = new Vector3(5, 0, 0)
            };
        }
    }
}
