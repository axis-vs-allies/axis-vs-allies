using Sandbox;

// Game.cs

public partial class AvaGame : Sandbox.Game
{
	public HUD gameHUD;

	public AvaGame()
	{
		if ( !IsClient ) gameHUD = new HUD();
	}

	[Event.Hotload]
	public void HotloadUpdate()
	{
		if ( !IsClient || gameHUD == null ) return;
		gameHUD?.Delete();
		gameHUD = new HUD();
	}

	public override void ClientJoined( Client client )
	{
		base.ClientJoined( client );

		// Create a pawn and assign it to the client.
		var player = new MyPlayer();
		client.Pawn = player;

		player.Respawn();
	}
}
