using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

public partial class Vitals : Panel
{
	private Label Health;
	private Panel HealthBar;

	public Vitals()
	{
		StyleSheet.Load( "/ui/Vitals.scss" );

		Panel healthBg = Add.Panel( "healthBg" );

		Panel healthIconBg = healthBg.Add.Panel( "healthIconBg" );
		healthIconBg.Add.Label( "favorite", "healthIcon" );

		Panel healthBarBg = healthBg.Add.Panel( "healthBarBg" );
		HealthBar = healthBarBg.Add.Panel( "healthBar" );

		Health = healthBg.Add.Label( "0", "healthText" );
	}

	public override void Tick()
	{
		base.Tick();

		var player = Local.Pawn;
		if ( player == null ) return;

		Health.Text = $"{player.Health.CeilToInt()}";

		HealthBar.Style.Dirty();
		HealthBar.Style.Width = Length.Percent( player.Health );
	}
}
