using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

public partial class MOTD : Panel
{
	private bool IsOpen = false;
	private TimeSince LastOpen;

	public MOTD()
	{
		StyleSheet.Load( "/ui/MOTD.scss" );

		Panel userMenuPanel = Add.Panel( "menu" );
	}

	public override void Tick()
	{
		base.Tick();

		if ( Input.Pressed( InputButton.Menu ) && LastOpen >= .1f )
		{
			IsOpen = !IsOpen;
			LastOpen = 0;
		}

		SetClass( "open", IsOpen );
	}
}
