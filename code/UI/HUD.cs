using Sandbox;
using Sandbox.UI;

public partial class HUD : HudEntity<RootPanel>
{
	public HUD()
	{
		if ( !IsClient ) return;

		RootPanel.AddChild<Vitals>();
		RootPanel.AddChild<MOTD>();
	}
}
